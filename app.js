const express = require('express')
const bodyParser = require('body-parser')
const app = express()

app.use(bodyParser.json())

const path = require('path')
const db = require ('./db')
const collPlanes = 'planes'
const collUsuarios = 'usuarios'
const collSales = 'sales'
const router = express.Router()


router.get('/plan', (req, res) => {
    res.sendFile(path.join(__dirname, './views/plan.html'))
    
})

router.get('/rendimiento', (req, res) => {
    res.sendFile(path.join(__dirname, './views/rendimiento.html'))
    
})

router.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, './views/index.html'))
})



app.post('/login', (req, res) => {
    const entrada = req.body;
       
    db.getDB().collection(collUsuarios).findOne(entrada, (err, result) => {
        if(err)
            console.log(err);
        else
            res.json(result)
        
        
    })
})



// Planes
app.get('/getPlanes', (req, res) => {
    db.getDB().collection(collPlanes).find({}).toArray((err, documents) => {
        if(err)
            console.log(err);
        
        else
            res.json(documents)
        
    })
})

app.get('/getPlan/:id', (req, res) => {
    const planID = req.params.id
    db.getDB().collection(collPlanes).find({_id : db.getPrimaryKey(planID)}).toArray((err, documents) => {
        if(err)
            console.log(err);
            
        else
            res.json(documents)
        
    })
})

app.put('/plan/:id', (req, res) => {
    const planID = req.params.id
    const plan = req.body
    const planObject = {
        nombrePlan : plan.nombrePlan,
        invMax : plan.invMax,
        invMin : plan.invMin,
        tasaMen : plan.tasaMen,
        duracion : plan.duracion
    }
    db.getDB().collection(collPlanes).findOneAndUpdate({_id : db.getPrimaryKey(planID)}, {$set : planObject}, {returnOriginal : false}, (err, result) => {
        if(err)
            console.log(err);
        
        else
            res.json(result)
        
    })
})

app.post('/plan', (req, res) => {
    const entrada = req.body;
       
    db.getDB().collection(collPlanes).insertOne(entrada, (err, result) => {
        if(err)
            console.log(err);
        else
            res.json({result : result, document : result.ops[0]})
    })
})

app.delete('/plan/:id', (req, res) => {
    const planID = req.params.id;
    db.getDB().collection(collPlanes).findOneAndDelete({_id : db.getPrimaryKey(planID)}, (err, result) => {
        if(err)
            console.log(err);
        else{
            res.json(result)
        }
            
    })
})


// DATABASE

db.connect((err) => {
    if(err){
        console.log('Error en la conexion')
        process.exit(1)
    }
    else{
        app.use('/', router)
        app.listen(3000, () => {
            console.log('Conecto a la base de datos, Puerto 3000');
            
        })
    }
})